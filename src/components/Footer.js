import React from 'react'
import NouvelItem from './NouvelItem'
import Total from './Total'

class Footer extends React.Component {


    render(){
        return (
            <div className="Footer">
                <NouvelItem costs={this.props.costs} addthing={this.props.addthing} />
                <Total costs={this.props.costs} />
            </div>
        )
    }
}

export default Footer