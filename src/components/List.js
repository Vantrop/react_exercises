import React from 'react'
import Table from 'react-bootstrap/Table'
import Row from 'react-bootstrap/Row'


class List extends React.Component {

    render(){
        let list = (
            <Table striped bordered hover size="sm">
                <tbody>
                    {this.props.costs.map((items)=>
                            <tr key={items.title+items.paidBy}>
                                <td>{items.title}</td>
                                <td>{items.paidBy}</td>
                                <td>{items.amount}</td>
                            </tr>
                        )}
                </tbody>
            </Table>
        );
        return(
        <Row noGutters>
            {list}
        </Row>
    )}
}

export default List