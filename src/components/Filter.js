import React from 'react'
import Row from 'react-bootstrap/Row'
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'


class ButtonList extends React.Component {
    
    constructor(props){
        super(props)
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e){
        this.props.filterclick(this.props.name)
        //console.log(this.props.costs)
    }

    render(){
        return(
            <Dropdown.Item onClick={this.handleClick}>{this.props.name}</Dropdown.Item>
        )
    }
}


class Filter extends React.Component {

    name = "select a name to filter"

    render(){

        return (
            <Row noGutters>
                <h2>Filter : </h2>
                <DropdownButton id="dropdown-basic-button" title={this.name}>
                    <ButtonList key={"no filter"} filterclick={this.props.filterclick} name={"no filter"} />
                    {this.props.users.map((items)=>
                        <ButtonList key={items} filterclick={this.props.filterclick} name={items} />
                        
                    )}
                </DropdownButton>
            </Row>
        )
    }
}
export default Filter