import React from 'react'
import Row from 'react-bootstrap/Row'


class Total extends React.Component {

    total(){
        let res = 0
        this.props.costs.forEach(element => {
            res+=element.amount
        });
        return res
    }


    render(){
        return (
            <Row noGutters className="Total">
                {this.total()}
            </Row>
        )
    }
}

export default Total