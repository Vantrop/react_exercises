import React from 'react'
import Row from 'react-bootstrap/Row'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'



class NouvelItem extends React.Component {

    constructor(props) {
        super(props)
        this.state={
            paidBy: '',
            title:'',
            amount:''
        }
        this.handleChangePaidBy = this.handleChangePaidBy.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChangeAmount = this.handleChangeAmount.bind(this)
        this.handleChangeTitle =this.handleChangeTitle.bind(this)
    }

    handleChangePaidBy(event) {
        this.setState({paidBy: event.target.value});
    }
    handleChangeAmount(event) {
        this.setState({amount: event.target.value});
    }
    handleChangeTitle(event) {
        this.setState({title: event.target.value});
    }

    handleSubmit(event) {
        
        console.log(this.state)
        this.props.addthing(this.state)
        event.preventDefault();
    }


    render(){
        return(
            
                <Form onSubmit={this.handleSubmit}>
                    <Row noGutters>
                        <Col >
                            <Form.Control onChange={this.handleChangeTitle} value={this.state.title} placeholder="Item" />
                        </Col>
                        <Col >
                            <Form.Control onChange={this.handleChangePaidBy} value={this.state.paidBy} placeholder="Person" />
                        </Col>
                        <Col >
                            <Form.Control onChange={this.handleChangeAmount} value={this.state.amount} placeholder="amount" />
                        </Col>
                        <Col >
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Col>
                    </Row>
                </Form>
           
        )
    }
}

export default NouvelItem