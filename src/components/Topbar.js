import React from 'react';
import Filter from './Filter';
import Row from 'react-bootstrap/Row'


class Topbar extends React.Component {
    

    render() {
      return (
        <div className="Topbar">
          <Row noGutters>
            <h1>{this.props.appName}</h1>
          </Row>
          <Filter filterclick={this.props.filterclick} users={this.props.users}/>
        
          
        </div>
      );
    }
}

export default Topbar