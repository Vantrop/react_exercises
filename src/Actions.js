
const ADD_ITEM = 'ADD_ITEM'
const FILTER_LIST = 'FILTER_LIST'


export const addItem = (paidBy, title, amount) => {
    return { type: ADD_ITEM, item: { paidBy: paidBy, title: title, amount: amount}}
}
export const boundAddItem = (paidBy, title, amount) => dispactch(addItem(paidBy,title,amount))


export const filterList = (name) => {
    return { type: FILTER_LIST, name: name}
}
export const boundFilterList = (name) => dispactch(filterList(name))

