import React from 'react';
import './App.css';
import Topbar from './components/Topbar'
import List from './components/List.js'
import Footer from './components/Footer'
import 'bootstrap/dist/css/bootstrap.min.css';

const users = ['Amine', 'Julie', 'Kévin']
let costs = [{paidBy: 'Amine', title: 'Beer', amount: 15},
{paidBy: 'Julie', title: 'Beer', amount: 15},
{paidBy: 'Kévin', title: 'Beer', amount: 15}]

const appName = "Nom de l'appli"

class App extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      nonfilterd: costs,
      filtered: costs
    }
    this.changeFilter = this.changeFilter.bind(this);
    this.addThing = this.addThing.bind(this)
  }


  changeFilter(name){
    this.setState({
      nonfilterd: costs,
      filtered: costs.filter((value) =>{
        if (name === "no filter"){
            return true
        }else{
          
            return name === value.paidBy
        }})
    })
  }

  addThing(obj) {
    let list = this.state.nonfilterd
    list.push({paidBy:obj.paidBy,title:obj.title,amount:Number.parseFloat(obj.amount)})
    this.setState({
      nonfilterd: list
    })
   // this.state.nonfilterd.push()
  }

  render(){
    return (
      <div className="App">
        <Topbar users={users} appName={appName} filterclick={this.changeFilter} />
        <List costs={this.state.filtered} />
        <Footer costs={this.state.nonfilterd} addthing={this.addThing}/>
      </div>
    );
  }
}

export default App;
